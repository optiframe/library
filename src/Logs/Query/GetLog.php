<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Query;

use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Interface\QueryInterface;

class GetLog implements QueryInterface
{
    private string $collection;
    private Id $id;

    private const HANDLER = \OptiFrame\Library\Logs\Handler\GetLogHandler::class;

    public function __construct(
        string $collection,
        Id $id
    )
    {
        $this->collection = $collection;
        $this->id = $id;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getCollection(): string
    {
        return $this->collection;
    }

    public function getId(): Id
    {
        return $this->id;
    }
}