<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cache;

use OptiFrame\Library\Cache\Command\SaveCache;
use OptiFrame\Library\Cache\Query\GetCache;
use OptiFrame\Library\Handler\HandleTrait;
use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class Cache
{
    use HandleTrait;

    private string $storageId;
    private ?int $ttl;

    private null|array|object $storage;
    private bool $switchSave;

    /** @var string cache collection id */
    private const CACHE_COLLECTION = 'cache';

    /** @var string logs saves providers list */
    private const CACHE_SAVES_PROVIDERS = [
        'File' => \OptiFrame\Library\Provider\NoSQL\FileProvider::class,
        'Redis' => \OptiFrame\Library\Provider\NoSQL\RedisProvider::class
    ];

    public function __construct(string $storageId, int $ttl = null)
    {
        $this->storageId = new Id($storageId);
        $this->ttl = $ttl;
        $this->extractCacheFromMemory();
    }

    public function get(): null|array|object
    {
        return $this->storage;
    }

    public function set(null|array|object $storage): void
    {
        $this->storage = $storage;
    }

    public function clear(): void
    {
        $this->switchSave = true;
        $this->storage = null;
    }

    public function save(): void
    {
        $this->handle(
            new SaveCache(
                self::CACHE_COLLECTION,
                $this->storageId,
                $this->storage,
                $this->hasBeenExisted
            ),
            $this->getProvider()
        );
        $this->switchSave = false;
    }

    public function __destruct()
    {
        if ($this->switchSave) {
            $this->save();
        }
    }

    private function extractCacheFromMemory(): void
    {
        $this->storage =  $this->handle(
            new GetCache(
                self::CACHE_COLLECTION,
                $this->storageId
            ),
            $this->getProvider()
        );
    }

    /** @return NoSQLProviderInterface return valid provider */
    private function getProvider(): NoSQLProviderInterface
    {
        $provider = self::CACHE_SAVES_PROVIDERS[$_ENV['CACHE_SAVES_PROVIDER']];
        return new $provider(self::CACHE_COLLECTION, null, $this->ttl);
    }
}