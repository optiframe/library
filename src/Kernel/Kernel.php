<?php

declare(strict_types=1);

namespace OptiFrame\Library\Kernel;

abstract class Kernel
{
    private Extension $extension;
    private const EXTENSIONS_PATH = '/config/extensions.inc';

    public function __construct(string $extension)
    {
        $class = $this->getExtensionsList()[$extension];
        $this->extension = new $class;
    }

    public function run(string $method): void
    {
        $this->extension->{$method}();
    }

    public function terminate(): void
    {
        $this->extension->terminate();
    }

    private function getExtensionsList(): array
    {
        return include \APP_PATH . self::EXTENSIONS_PATH;
    }
}