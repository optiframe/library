<?php

declare(strict_types=1);

namespace OptiFrame\Library\Kernel;

abstract class Extension
{
    abstract function terminate(): void;
}