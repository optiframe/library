<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Command;

use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Interface\CommandInterface;
use OptiFrame\Library\Logs\DTO\Log;

class SaveLog implements CommandInterface
{
    private string $collection;
    private Log $log;

    private const HANDLER = \OptiFrame\Library\Logs\Handler\SaveLogHandler::class;

    public function __construct(
        string $collection,
        Log $log
    )
    {
        $this->collection = $collection;
        $this->log = $log;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getCollection(): string
    {
        return $this->collection;
    }

    public function getId(): Id
    {
        return $this->log->getId();
    }

    public function getLog(): Log
    {
        return $this->log;
    }

}