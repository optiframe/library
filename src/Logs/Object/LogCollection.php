<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Object;

class LogCollection
{
    private array $storage = [];

    public function set(string $key, mixed $value): void
    {
        $this->storage[$key] = $value;
    }

    public function get(string $key): mixed
    {
        return $this->storage[$key];
    }

    public function push(mixed $value): void
    {
        $this->storage[] = $value;
    }

    public function insert(mixed $value): void
    {
        $this->push($value);
    }

    public function getStorage(): array
    {
        return $this->storage;
    }
}