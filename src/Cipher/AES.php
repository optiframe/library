<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher;

use OptiFrame\Library\Cipher\Object\AESSecretKey;
use OptiFrame\Library\Cipher\Provider\AESProvider;
use OptiFrame\Library\Cipher\Query\DecryptByAES;
use OptiFrame\Library\Cipher\Query\EncryptByAES;
use OptiFrame\Library\Handler\HandleTrait;

class AES
{
    use HandleTrait;

    private AESSecretKey $secretKey;
    private string $cipherAlgorithm;

    private const DEFAULT_CIPHER_ALGORITHM = 'aes-256-cbc';

    public function __construct(?AESSecretKey $secretKey = null, ?string $cipherAlgorithm = null)
    {
        $this->secretKey = $secretKey ?? new AESSecretKey();
        $this->cipherAlgorithm = $cipherAlgorithm ?? self::DEFAULT_CIPHER_ALGORITHM;
    }

    public function encrypt(string $string, bool $isReturnAsBase64encode = false, ?string $key = null): ?string
    {
        return $this->handle(
            new EncryptByAES(
                $string,
                $isReturnAsBase64encode,
                $key
            ),
            new AESProvider(
                $this->cipherAlgorithm,
                (string) $this->secretKey
            )
        );
    }

    public function decrypt(string $cipher, bool $isBase64encoded = false, ?string $key = null): ?string
    {
        return $this->handle(
            new DecryptByAES(
                $cipher,
                $isBase64encoded,
                $key
            ),
            new AESProvider(
                $this->cipherAlgorithm,
                (string) $this->secretKey
            )
        );
    }
}