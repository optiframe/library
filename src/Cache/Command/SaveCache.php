<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cache\Command;

use OptiFrame\Library\Handler\GetHandlerTrait;
use OptiFrame\Library\Interface\CommandInterface;

class SaveCache implements CommandInterface
{
    use GetHandlerTrait;
    private const HANDLER = \OptiFrame\Library\Cache\Handler\SetCacheHandler::class;

    public function __construct(
        private string $collection,
        private string $id,
        private array|object $resource
    ) {}

    public function getCollection(): string
    {
        return $this->collection;
    }

    public function getId(): string
    {
        return $this->Id;
    }

    public function getResource(): array|object
    {
        return $this->resource;
    }
}