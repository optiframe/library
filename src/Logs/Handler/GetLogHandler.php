<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Handler;

use OptiFrame\Library\Logs\DTO\Log;
use OptiFrame\Library\Logs\Query\GetLog;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface as Provider;

class GetLogHandler
{
    private Provider $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(GetLog $query): ?Log
    {
        return $this->provider->getObject(
            $query->getCollection(),
            (string) $query->getId()
        );
    }
}