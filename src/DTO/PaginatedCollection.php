<?php

declare(strict_types=1);

namespace OptiFrame\Library\DTO;

class PaginatedCollection extends Collection
{
    private int $pageTotal;
    private int $pageNumber;
    private int $pageSize;

    private const DEFAULT_PAGE_SIZE = 100;

    public function __construct(array $items = [], ?int $count = null, int $pageNumber, int $pageTotal, ?int $pageSize = null)
    {
        $this->items = $items;
        $this->count = $count ?? count($items);
        $this->pageNumber = $pageNumber;
        $this->pageTotal = $pageTotal;
        $this->pageSize = $pageSize ?? self::DEFAULT_PAGE_SIZE;
    }

    public function getPageTotal(): int
    {
        return $this->pageTotal;
    }
    
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }
}