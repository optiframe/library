<?php

declare(strict_types=1);

namespace OptiFrame\Library\Handler;

trait GetHandlerTrait
{
    public function getHandler(): string
    {
        return self::HANDLER;
    }
}