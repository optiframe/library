<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Handler;

use OptiFrame\Library\Cipher\Provider\CipherProviderInterface as Provider;
use OptiFrame\Library\Cipher\Query\EncryptByAES;

class EncryptByAESHandler
{
    private Provider $provider;

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(EncryptByAES $query): ?string
    {
        return $this->provider->encrypt(
            $query->getString(),
            $query->isReturnAsBase64encode(),
            $query->getKey()
        );
    }
}