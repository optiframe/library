<?php

declare(strict_types=1);

namespace OptiFrame\Library;

use Exception;
use OptiFrame\Library\Kernel\Kernel;
use OptiFrame\Library\Logs\Logs;

class Application extends Kernel
{
    private Logs $logs;

    public function __construct(string $extension)
    {
        $this->logs = new Logs();
        $this->logs->info->set('startTime', \APP_START);
        $this->logs->info->set('running', $extension);
        parent::__construct($extension);
    }

    public function run(string $method): void
    {
        $this->logs->info->set('method', $method);
        parent::run($method);
    }

    public function terminate(): void
    {
        parent::terminate();
        $this->logs->info->set('responseTime', microtime(true));
        $this->logs->info->set('requestTime', $_SERVER['REQUEST_TIME_FLOAT'] ?? \APP_START);
    }

    public function __destruct()
    {
        $this->logs->info->set('endTime', microtime(true));
        if (!$this->logs->save()) {
            throw new Exception('The application operation log has not been saved');
        }
    }
}
