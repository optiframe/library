<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Handler;

use OptiFrame\Library\Logs\Command\SaveLog;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface as Provider;

class SaveLogHandler
{
    private Provider $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(SaveLog $command): bool
    {
        return $this->provider->insertObject(
            $command->getCollection(),
            (string) $command->getId(),
            $command->getLog()
        );
    }
}