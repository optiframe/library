<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cache\Handler;

use OptiFrame\Library\Cache\Query\GetCache;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class GetCacheHandler
{
    private NoSQLProviderInterface $provider;

    public function __construct(NoSQLProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(GetCache $query): null|array|object
    {
        return $this->provider->getObject(
            $query->getCollection(),
            $query->getId()
        );
    }
}