<?php

declare(strict_types=1);

namespace OptiFrame\Library\Entity;

use OptiFrame\Library\Exception\Exception;
use OptiFrame\Library\Logs\Logs;

class File
{
    private string $path;
    private string $absolutePath;
    private string $content = '';
    private int $contentLength = 0;
    private bool $isExists;

    public function __construct(string $path)
    {
        $this->setPaths($path);
        if ($this->isExists = file_exists($this->absolutePath)) {
            $this->content = file_get_contents($this->absolutePath);
            $this->contentLength = strlen($this->content);
        }
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getAbsolutePath(): string
    {
        return $this->absolutePath;
    }

    public function getContent(): ?string
    {
        if ($this->contentLength === 0) {
            return null;
        }
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
        $this->contentLength = strlen($content);
    }

    public function putContent(): bool
    {
        $this->createDirectoriesIfNotExists();
        $exists = $this->isExists;
        if (file_put_contents($this->absolutePath, $this->content, LOCK_EX)) {
            (new Logs())->events->push(sprintf($exists ? 'New content (%d bytes) put into %s' : 'File created and put content (%d bytes) in %s', $this->contentLength, $this->path));
            $this->isExists = true;
            return true;
        }
        throw new Exception(sprintf('The file could not be created in %s', $this->path));
        return false;
    }

    public function isExists(): bool
    {
        return $this->isExists;
    }

    public function delete(): bool
    {
        if (unlink($this->absolutePath)) {
            $this->isExists = false;
            $this->content = '';
            $this->contentLength = 0;
            return true;
        }
        return false;
    }
    
    private function setPaths(string $path): void
    {
        if ($pos = strpos($path, \APP_PATH) !== false) {
            $path = substr($path, $pos + strlen(\APP_PATH) - 1);   
        }
        $this->path = $path;
        $this->absolutePath = \APP_PATH . $path;
    }
    
    private function createDirectoriesIfNotExists(): void
    {
        $path = substr($this->absolutePath, 0, strrpos($this->absolutePath, '/'));
        @mkdir($path, 0777, true);
    }
}