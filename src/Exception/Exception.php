<?php

declare(strict_types=1);

namespace OptiFrame\Library\Exception;

use Exception as GlobalException;
use OptiFrame\Library\Logs\Logs;
use Throwable;

class Exception extends GlobalException
{
    public function __construct(string $message = "" , int $code = 0 , Throwable $previous = null )
    {
        parent::__construct($message, $code, $previous);
        (new Logs())->exceptions->push([
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
            'file' => $this->getFile(),
            'line' => $this->getLine(),
            'trace' => $this->getTrace(),
            'previous' => $this->getPrevious()
        ]);
    }
}