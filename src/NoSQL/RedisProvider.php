<?php

declare(strict_types=1);

namespace OptiFrame\Library\Provider\NoSQL;

use OptiFrame\Library\DTO\Collection;
use OptiFrame\Library\Redis\Redis;

class RedisProvider implements NoSQLProviderInterface
{
    private string $collection;
    private string $namespace;
    private ?int $ttl;

    private Redis $redis;

    public function __construct(?string $collection = null, ?string $namespace = null, ?int $ttl = null, ?Redis $redis = null)
    {
        $this->collection = $collection ?? 'local';
        $this->namespace = $namespace ?? 'var';
        $this->ttl = $ttl;

        $this->redis = $redis ?? new Redis($_ENV['REDIS_LOCAL_HOST'], $_ENV['REDIS_LOCAL_PORT']);
    }

    public function setString(?string $collection = null, string $key, string $string): bool
    {
        $params = $this->ttl ? ['ex' => $this->ttl] : [];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function insertString(?string $collection = null, string $key, string $string): bool
    {
        $params = $this->ttl ? ['nx', 'ex' => $this->ttl] : ['nx'];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function updateString(?string $collection = null, string $key, string $string): bool
    {
        $params = $this->ttl ? ['xx', 'ex' => $this->ttl] : ['xx'];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function deleteString(?string $collection = null, string $key): bool
    {
        return $this->redis->del($this->getKey($collection, $key));
    }

    public function getString(?string $collection = null, string $key): null|string
    {
        if ($output = $this->redis->get($this->getKey($collection, $key))) {
            return $output;
        }
        return null;
    }


    public function setObject(?string $collection = null, string $key, array|object $object): bool
    {
        if (!is_array($object)) {
            $string = serialize($object);
        }
        else {
            $string = json_encode($object);
        }

        $params = $this->ttl ? ['ex' => $this->ttl] : [];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function insertObject(?string $collection = null, string $key, array|object $object): bool
    {
        if (!is_array($object)) {
            $string = serialize($object);
        }
        else {
            $string = json_encode($object);
        }

        $params = $this->ttl ? ['nx', 'ex' => $this->ttl] : ['nx'];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function updateObject(?string $collection = null, string $key, array|object $object): bool
    {
        if (!is_array($object)) {
            $string = serialize($object);
        }
        else {
            $string = json_encode($object);
        }

        $params = $this->ttl ? ['xx', 'ex' => $this->ttl] : ['xx'];
        return $this->redis->set($this->getKey($collection, $key), $string, $params);
    }

    public function deleteObject(?string $collection = null, string $key): bool
    {
        return $this->redis->del($this->getKey($collection, $key));
    }

    public function getObject(?string $collection = null, string $key): null|array|object
    {
        $output = $this->redis->get($this->getKey($collection, $key));
        if (empty($output) || !$output) {
            return null;
        }
        if ($this->isSerialized($output)) {
            return unserialize($output);
        }
        if ($output = json_decode($output, true)) {
            return $output;
        }
        return null;
    }


    // public function insertCollection(?string $collectionName, Collection $collection): bool;

    // public function updateCollection(?string $collectionName, Collection $collection): bool;

    public function deleteCollection(?string $collectionName, ?Collection $collection = null): bool
    {
        $pattern = $this->getKey($collectionName);
        $keys = $this->redis->keys($pattern . '*');
        if (empty($keys)) {
            return false;
        }
        if ($collection === null) {
            return (bool) $this->redis->del($keys);
        }
        return (bool) $this->redis->del($collection->getItems());
    }

    public function getCollection(?string $collectionName = null): Collection
    {
        $pattern = $this->getKey($collectionName);
        $items = [];

        $keys = $this->redis->keys($pattern . '*');
        $values = $this->redis->mGet($keys);

        $patternLen = strlen($pattern);

        foreach ($keys as $id => $key) {
            $value = $values[$id];
            $key = substr($key, $patternLen);
            if ($this->isSerialized($value)) {
                $items[$key] = unserialize($value);
                continue;
            }
            if ($output = json_decode($value, true)) {
                $items[$key] = $output;
                continue;
            }
            if (is_string($value)) {
                $items[$key] = $value;
                continue;
            }
            if (empty($value) || !$value) {
                $items[$key] = null;
                continue;
            }
        }

        return new Collection($items, count($items));
    }


    // public function getPaginatedCollection(?string $collectionName, ): PaginatedCollection;


    private function getKey(?string $collection = null, string $key = ''): string
    {
        return implode('_', [
            $this->namespace,
            $collection ?? $this->collection,
            $key
        ]);
    }

    private function isSerialized($data, $strict = true)
    {
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            if (false === $semicolon && false === $brace)
                return false;
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }
}