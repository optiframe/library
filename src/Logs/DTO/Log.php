<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\DTO;

use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Logs\Object\LogCollection;

class Log
{
    /** @var Id */
    private Id $_id;

    /** @var LogCollection[] */
    private array $storage;

    /**
     * @param Id $id
     * @param LogCollection[] $logCollections
     */
    public function __construct(Id $id, array $logCollections)
    {
        $this->_id = $id;
        $this->storage = $logCollections;
    }

    /** @return Id */
    public function getId(): Id
    {
        return $this->_id;
    }

    /** @return LogCollection[] */
    public function getLogCollections(): array
    {
        return $this->storage;
    }
    
    /** @return array */
    public function getStorage(): array
    {
        $output = [];
        foreach ($this->storage as $key => $logCollection) {
            $output[$key] = $logCollection->getStorage();
        }
        return $output;
    }

    /** @return array */
    public function getAsArray(): array
    {
        return [
            '_id' => (string) $this->_id,
            'storage' => $this->getStorage()
        ];
    }
}