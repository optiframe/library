<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Query;

use OptiFrame\Library\Interface\QueryInterface;

class DecryptByAES implements QueryInterface
{
    private string $cipher;
    private bool $isBase64encoded;
    private ?string $key;

    private const HANDLER = \OptiFrame\Library\Cipher\Handler\DecryptByAESHandler::class;

    public function __construct(
        string $cipher,
        bool $isBase64encoded,
        ?string $key = null
    )
    {
        $this->cipher = $cipher;
        $this->isBase64encoded = $isBase64encoded;
        $this->key = $key;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getCipher(): string
    {
        return $this->cipher;
    }

    public function isBase64encoded(): bool
    {
        return $this->isBase64encoded;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }
}