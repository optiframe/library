<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Query;

use OptiFrame\Library\Interface\QueryInterface;

class GetLogCollection implements QueryInterface
{
    private string $collection;

    private const HANDLER = \OptiFrame\Library\Logs\Handler\GetLogCollectionHandler::class;

    public function __construct(string $collection)
    {
        $this->collection = $collection;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getCollection(): string
    {
        return $this->collection;
    }
}