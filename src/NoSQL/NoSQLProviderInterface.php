<?php

declare(strict_types=1);

namespace OptiFrame\Library\Provider\NoSQL;

use OptiFrame\Library\DTO\Collection;
use OptiFrame\Library\Interface\ProviderInterface;

interface NoSQLProviderInterface extends ProviderInterface
{
    /** 
     * @param null|string $collection to identify collection to set resource.
     * @param string $key to identify resource inside a collection.
     * @param string $string to set into a database.
     * 
     * @return bool The *true* if success, the *false* if failed.
     */
    public function setString(?string $collection = null, string $key, string $string): bool;

    /** 
     * @param null|string $collection to identify collection to insert resource.
     * @param string $key to identify resource inside a collection.
     * @param string $string to insert into a database.
     * 
     * @return bool The *true* if success, the *false* if failed.
     */
    public function insertString(?string $collection = null, string $key, string $string): bool;

    /** 
     * @param null|string $collection to identify collection to update resource.
     * @param string $key to identify resource inside a collection.
     * @param string $string to update into a database.
     * 
     * @return bool The *true* if success, the *false* if failed.
     */
    public function updateString(?string $collection = null, string $key, string $string): bool;
    
    /** 
     * @param null|string $collection to identify collection to delete resource.
     * @param string $key to identify resource inside a collection.
     * 
     * @return bool The *true* if deleted, the *false* if failed.
     */
    public function deleteString(?string $collection = null, string $key): bool;

    /** 
     * @param null|string $collection to identify collection to get resource.
     * @param string $key to identify resource inside a collection.
     * 
     * @return null|string Return *null* if resource doesn't exists or try failed. Return *string* if success.
     */
    public function getString(?string $collection = null, string $key): null|string;

    /**
     * 
     */
    public function setObject(?string $collection = null, string $key, array|object $object): bool;

    /**
     * 
     */
    public function insertObject(?string $collection = null, string $key, array|object $object): bool;

    /**
     * 
     */
    public function updateObject(?string $collection = null, string $key, array|object $object): bool;

    /**
     * 
     */
    public function deleteObject(?string $collection = null, string $key): bool;

    /**
     * 
     */
    public function getObject(?string $collection = null, string $key): null|array|object;

    /**
     * 
     */
    // public function insertCollection(?string $collectionName, Collection $collection): bool;

    /**
     * 
     */
    // public function updateCollection(?string $collectionName, Collection $collection): bool;

    /**
     * 
     */
    public function deleteCollection(?string $collectionName, ?Collection $collection = null): bool|Collection;

    /**
     * 
     */
    public function getCollection(?string $collectionName): ?Collection;

    /**
     * 
     */
    // public function getPaginatedCollection(?string $collectionName, int $pageNumber, ?int $pageSize = null, bool $reverseOrder = false): PaginatedCollection;

}