<?php

declare(strict_types=1);

namespace OptiFrame\Library\Provider\NoSQL;

use OptiFrame\Library\DTO\Collection;
use OptiFrame\Library\Entity\File;

class FileProvider implements NoSQLProviderInterface
{
    private string $collection;
    private string $path;

    private const JSON_EXT = '.json';
    private const OBJECT_EXT = '.obj';
    private const STRING_EXT = '.txt';

    public function __construct(string $collection, ?string $namespace = null)
    {
        $this->collection = $collection;
        $this->path = '/var/' . $namespace ?? 'local';
    }

    public function insertString(?string $collection = null, string $key, string $string): bool
    {
        $file = new File($this->getPath($collection) . $key . self::STRING_EXT);
        if ($file->isExists()) {
            return false;
        }
        $file->setContent($string);
        return $file->putContent();
    }

    public function updateString(?string $collection = null, string $key, string $string): bool
    {
        $file = new File($this->getPath($collection) . $key . self::STRING_EXT);
        if ($file->isExists()) {
            $file->setContent($string);
            return $file->putContent();
        }
        return false;
    }

    public function deleteString(?string $collection = null, string $key): bool
    {
        $file = new File($this->getPath($collection) . $key . self::STRING_EXT);
        if (!$file->isExists()) {
            return false;
        }
        return $file->delete();
    }

    public function getString(?string $collection = null, string $key): null|string
    {
        $file = new File($this->getPath($collection) . $key . self::STRING_EXT);
        return $file->getContent();
    }


    public function insertObject(?string $collection = null, string $key, array|object $object): bool
    {
        if (!is_array($object)) {
            $file = new File($this->getPath($collection) . $key . self::OBJECT_EXT);
            if ($file->isExists()) {
                return false;
            }
            $file->setContent(serialize($object));
            return $file->putContent();
        }
        $file = new File($this->getPath($collection) . $key . self::JSON_EXT);
        if ($file->isExists()) {
            return false;
        }
        $file->setContent(json_encode($object));
        return $file->putContent();
    }

    public function updateObject(?string $collection = null, string $key, array|object $object): bool
    {
        if (!is_array($object)) {
            $file = new File($this->getPath($collection) . $key . self::OBJECT_EXT);
            $file->setContent(serialize($object));
            return $file->putContent();
        }
        $file = new File($this->getPath($collection) . $key . self::JSON_EXT);
        $file->setContent(json_encode($object));
        return $file->putContent();
    }

    public function deleteObject(?string $collection = null, string $key): bool
    {
        $obj = new File($this->getPath($collection) . $key . self::OBJECT_EXT);
        $json = new File($this->getPath($collection) . $key . self::JSON_EXT);
        if ($json->isExists()) {
            return $json->delete();
        }
        if ($obj->isExists()) {
            return $obj->delete();
        }
        return false;
    }

    public function getObject(?string $collection = null, string $key): null|array|object
    {
        $obj = new File($this->getPath($collection) . $key . self::OBJECT_EXT);
        $json = new File($this->getPath($collection) . $key . self::JSON_EXT);
        if ($json->isExists()) {
            return json_decode($json->getContent(), true);
        }
        if ($obj->isExists()) {
            return unserialize($obj->getContent());
        }
        return null;
    }


    // public function insertCollection()

    // public function updateCollection()

    public function deleteCollection(?string $collectionName, ?Collection $collection = null): bool|Collection
    {
        $path = $this->getPath($collectionName, false);
        if ($collection === null) {
            return rmdir($path);
        }

        $items = [];
        foreach($collection->getItems() as $key => $item) {
            if (is_int($key)) {
                $fileName = (string) $item ?? (string) $key;
            }
            else {
                $fileName = (string) $key;
            }

            $file = glob($path . '/' . $fileName . '*')[0];

            $items[$fileName] = unlink($file);
        }

        return new Collection($items, count($items));
    }

    public function getCollection(?string $collectionName = null): Collection
    {
        $path = $this->getPath($collectionName);
        $items = [];
        foreach (glob(\APP_PATH . $path . '*') as $file) {
            $fileExt = '.' . pathinfo($file)['extension'];
            $key = basename($file, $fileExt);
            $fileObject = new File($file);
            switch ($fileExt) {
                case self::STRING_EXT: {
                    $items[$key] = $fileObject->getContent();
                    break;
                }
                case self::JSON_EXT: {
                    $items[$key] = json_decode($fileObject->getContent(), true);
                    break;
                }
                case self::OBJECT_EXT: {
                    $items[$key] = unserialize($fileObject->getContent());
                    break;
                }
            }
        }

        return new Collection($items, count($items));
    }


    // public function getPaginatedCollection()

    
    private function getPath(?string $collection, bool $finalSlash = true): string
    {
        $path = $this->path . '/' . ($collection ?? $this->collection) . ($finalSlash ? '/' : '');
        return $path;
    }
}