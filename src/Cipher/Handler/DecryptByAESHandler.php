<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Handler;

use OptiFrame\Library\Cipher\Provider\CipherProviderInterface as Provider;
use OptiFrame\Library\Cipher\Query\DecryptByAES;

class DecryptByAESHandler
{
    private Provider $provider;

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(DecryptByAES $query): ?string
    {
        return $this->provider->decrypt(
            $query->getCipher(),
            $query->isBase64encoded(),
            $query->getKey()
        );
    }
}