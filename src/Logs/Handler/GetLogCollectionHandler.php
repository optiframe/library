<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs\Handler;

use OptiFrame\Library\DTO\Collection;
use OptiFrame\Library\Logs\Query\GetLogCollection;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface as Provider;

class GetLogCollectionHandler
{
    private Provider $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(GetLogCollection $query): ?Collection
    {
        return $this->provider->getCollection(
            $query->getCollection()
        );
    }
}