<?php

declare(strict_types=1);

namespace OptiFrame\Library\Object;

use OptiFrame\Library\Exception\Exception;

class Id
{
    private string $id;

    public function __construct(?string $id = null, string $prefix = 'id')
    {
        if (strlen($prefix) > 9) {
            throw new Exception('The prefix of the Id is to long. You can use max 9 characters.');
        }
        
        $label = [
            'prefix' => $prefix,
            'time' => time(),
            'block5' => substr(bin2hex(random_bytes(5)), 0, 5),
            'block4' => substr(bin2hex(random_bytes(4)), 0, 4),
            'blockEnd' => substr(bin2hex(random_bytes((9 - strlen($prefix)))), 0, (9 - strlen($prefix)))
        ];

        $this->id = $id ?? implode('-', $label);
    }

    public function __toString(): string
    {
        return $this->id;
    }
}