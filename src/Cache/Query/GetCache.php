<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cache\Query;

use OptiFrame\Library\Handler\GetHandlerTrait;
use OptiFrame\Library\Interface\QueryInterface;

class GetCache implements QueryInterface
{
    use GetHandlerTrait;
    private const HANDLER = \OptiFrame\Library\Cache\Handler\GetCacheHandler::class;

    public function __construct(
        private string $collection,
        private string $id
    ) {}

    public function getCollection(): string
    {
        return $this->collection;
    }

    public function getId(): string
    {
        return $this->id;
    }
}