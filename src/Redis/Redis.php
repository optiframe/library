<?php

declare(strict_types=1);

namespace OptiFrame\Library\Redis;

use Redis as RedisGlobal;

class Redis extends RedisGlobal
{
    private string $host;
    private int $port;

    public function __construct(
        ?string $host = null,
        ?int $port = null
    )
    {
        $this->host = $host ?? $_ENV['REDIS_LOCAL_HOST'];
        $this->port = $port ?? $_ENV['REDIS_LOCAL_PORT'];

        parent::__construct();
        $this->connect($this->host, $this->port);
    }
}
