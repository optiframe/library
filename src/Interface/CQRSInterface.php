<?php

declare(strict_types=1);

namespace OptiFrame\Library\Interface;

interface CQRSInterface
{
    /**
     * @return string $handler which is a class string to create new handler object.
     */
    public function getHandler(): string;
}