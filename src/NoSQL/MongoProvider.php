<?php

declare(strict_types=1);

namespace OptiFrame\Library\Provider\NoSQL;

use MongoDB\Client;
use MongoDB\Collection;
use OptiFrame\Library\DTO\Collection as DTOCollection;

class MongoProvider implements NoSQLProviderInterface
{
    private string $collection;
    private string $database;

    private Client $mongoDB;
    private Collection $mongoCollection;

    private const ARRAY = 'array';
    private const OBJECT = 'object';
    private const STRING = 'string';

    public function __construct(?string $collection, ?string $database = null)
    {
        $this->database = $database ?? 'local';
        $this->collection = $collection;

        $this->mongoDB = new Client($_ENV['MONGO_DB_LOCAL_HOST'] . ':' . (string) $_ENV['MONGO_DB_LOCAL_PORT']);
        $this->mongoCollection = $this->mongoDB->{$this->database}->{$this->collection};
    }

    public function setString(?string $collection = null, string $key, string $string): bool
    {
        return $this->getString($collection, $key) ? $this->updateString($collection, $key, $string) : $this->insertString($collection, $key, $string);
    }

    public function insertString(?string $collection = null, string $key, string $string): bool
    {
        if ($this->collection($collection)->insertOne([
            '_id' => $key,
            'type' => self::STRING,
            self::STRING => $string
        ])) {
            return true;
        }
        return false;
    }

    public function updateString(?string $collection = null, string $key, string $string): bool
    {
        if ($this->collection($collection)->updateOne(
            [
                '_id' => $key
            ],
            [
                'type' => self::STRING,
                self::STRING => $string
            ]
        )) {
            return true;
        }
        return false;
    }

    public function deleteString(?string $collection = null, string $key): bool
    {
        if ($this->collection($collection)->deleteOne(
            [
                '_id' => $key
            ]
        )) {
            return true;
        }
        return false;
    }

    public function getString(?string $collection = null, string $key): null|string
    {
        $output = $this->collection($collection)->findOne([
            '_id' => $key
        ])->bsonSerialize();

        return $output->message ?? null;
    }

    public function setObject(?string $collection = null, string $key, array|object $object): bool
    {
        return $this->getObject($collection, $key) ? $this->updateObject($collection, $key, $object) : $this->insertObject($collection, $key, $object);
    }

    public function insertObject(?string $collection = null, string $key, array|object $object): bool
    {
        $type = is_array($object) ? self::ARRAY : self::OBJECT;
        if ($this->collection($collection)->insertOne(
            [
                '_id' => $key,
                'type' => $type,
                $type => is_array($object) ? json_encode($object) : serialize($object)
            ]
        )) {
            return true;
        }
        return false;
    }

    public function updateObject(?string $collection = null, string $key, array|object $object): bool
    {
        $type = is_array($object) ? self::ARRAY : self::OBJECT;
        if ($this->collection($collection)->updateOne(
            [
                '_id' => $key
            ],
            [
                'type' => $type,
                $type => is_array($object) ? json_encode($object) : serialize($object)
            ]
        )) {
            return true;
        }
        return false;
    }

    public function deleteObject(?string $collection = null, string $key): bool
    {
        if ($this->collection($collection)->deleteOne(
            [
                '_id' => $key
            ]
        )) {
            return true;
        }
        return false;
    }

    public function getObject(?string $collection = null, string $key): null|array|object
    {
        $output = $this->collection($collection)->findOne([
            '_id' => $key
        ])->bsonSerialize();
            
        return $output->type === 'array' ? json_decode($output->array, true) : unserialize($output->object);
    }


    // public function insertCollection(?string $collectionName, Collection $collection): bool;
    // public function updateCollection(?string $collectionName, Collection $collection): bool;

    public function deleteCollection(?string $collectionName = null, ?DTOCollection $collection = null): bool|DTOCollection
    {
        if ($collection === null) {
            $this->collection($collectionName)->drop();
            return true;
        }
        $this->collection($collectionName)->deleteMany(
            [
                '_id' => $collection->getItems()
            ]
        );
        return true;
    }

    public function getCollection(?string $collectionName = null): ?DTOCollection
    {
        $cursor = $this->collection($collectionName)->find();
        $items = [];

        foreach($cursor as $document) {
            $doc = $document->bsonSerialize();
            $items[$doc->_id] = $doc->type === 'array' ? json_decode($doc->array, true) : ($doc->type === 'string' ? $doc->string : unserialize($doc->object));
        }

        return new DTOCollection($items, count($items)) ?? null;
    }


    // public function getPaginatedCollection(?string $collectionName, int $pageNumber, ?int $pageSize = null, bool $reverseOrder = false): PaginatedCollection;

    private function collection(?string $collection = null): Collection
    {
        if ($collection === null) {
            return $this->mongoDB->{$this->database}->{$this->mongoCollection};
        }
        return $this->mongoDB->{$this->database}->{$collection};
    }
}
