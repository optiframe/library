<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cache\Handler;

use OptiFrame\Library\Cache\Command\SaveCache;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class SetCacheHandler
{
    private NoSQLProviderInterface $provider;

    public function __construct(NoSQLProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(SaveCache $command): bool
    {
        return $this->provider->setObject(
            $command->getCollection(),
            $command->getId(),
            $command->getResource()
        );
    }
}