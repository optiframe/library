<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs;

use OptiFrame\Library\DTO\Collection;
use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Handler\HandleTrait;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;
use OptiFrame\Library\Logs\Command\SaveLog;
use OptiFrame\Library\Logs\DTO\Log;
use OptiFrame\Library\Logs\Object\LogCollection;
use OptiFrame\Library\Logs\Query\GetLog;
use OptiFrame\Library\Logs\Query\GetLogCollection;

class LogsCollector
{
    use HandleTrait;

    /** @var Log log safety id */
    private Log $log;
    /** @var string collection id with timestamp */
    private string $collection;

    /** @var string prefix of log id */
    private const LOG_PREFIX = 'log';
    /** @var string prefix of logs collection id */
    private const LOG_COLLECTION_PREFIX = 'logs_';
    /** @var string name of namespace to store all logs */
    private const LOG_NAMESPACE = 'logs';

    /** @var string logs saves providers list */
    private const LOGS_SAVES_PROVIDERS = [
        'File' => \OptiFrame\Library\Provider\NoSQL\FileProvider::class,
        'MongoDB' => \OptiFrame\Library\Provider\NoSQL\MongoProvider::class
    ];

    /** @param LogCollection[] $logCollections */
    public function __construct(?array $logCollections = null)
    {
        $this->collection = self::LOG_COLLECTION_PREFIX . date($_ENV['LOGS_SNAPSHOT_INTERVAL']);
        if ($logCollections) {
            $this->log = new Log(new Id(null, self::LOG_PREFIX), $logCollections);
        }
    }

    /** 
     * @return bool *true* if success, *false* if failed 
     */
    public function saveLog(): bool
    {
        $saveLog = new SaveLog($this->collection, $this->log);
        if ($this->handle($saveLog, $this->getProvider()) || $_ENV['LOGS_SAVES_PROVIDER'] === 'File') {
            return true;
        }

        $provider = self::LOGS_SAVES_PROVIDERS['File'];
        return $this->handle($saveLog, new $provider($this->collection, self::LOG_NAMESPACE));
    }

    /**
     * @param null|string $collection id of the collection. If *null* a collection will be taken from *$this->collection* 
     * @param Id $id id of the log
     * 
     * @return null|Log object of Log DTO class
     */
    public function getLog(string $collection = null, Id $id): ?Log
    {
        $getLog = new GetLog($collection ?? $this->collection, $id);
        $output = $this->handle($getLog, $this->getProvider());
        if ($output !== null || $_ENV['LOGS_SAVES_PROVIDER'] === 'File') {
            return $output;
        }

        $provider = self::LOGS_SAVES_PROVIDERS['File'];
        return $this->handle($getLog, new $provider($this->collection, self::LOG_NAMESPACE));
    }

    /**
     * @param null|string $collection id of the collection. If *null* a collection will be taken from *$this->collection* 
     * 
     * @return Collection object of Log DTO class collection
     */
    public function getLogCollection(string $collection = null): ?Collection
    {
        $getLogCollection = new GetLogCollection($collection ?? $this->collection);
        $output = $this->handle($getLogCollection, $this->getProvider());
        if ($_ENV['LOGS_SAVES_PROVIDER'] === 'File') {
            return $output;
        }

        $provider = self::LOGS_SAVES_PROVIDERS['File'];
        $filesOutput = $this->handle($getLogCollection, new $provider($this->collection, self::LOG_NAMESPACE));

        return new Collection(array_merge(
            $output->getItems() ?? null,
            $filesOutput->getItems() ?? null
        )) ?? null;
    }

    /** @return NoSQLProviderInterface return valid provider */
    private function getProvider(): NoSQLProviderInterface
    {
        $provider = self::LOGS_SAVES_PROVIDERS[$_ENV['LOGS_SAVES_PROVIDER']];
        return new $provider($this->collection, self::LOG_NAMESPACE);
    }
}