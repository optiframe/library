<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Provider;

use OptiFrame\Library\Interface\ProviderInterface;

interface CipherProviderInterface extends ProviderInterface
{
    /**
     * @param string $cipher is a byte/base64 string which You want to decrypt
     * @param bool $isBase64encoded is flag. If true cipher first be encoded
     * @param null|string $key if You would like to use custom key to decrypt
     * 
     * @return null|string *null* when decryption failed and *string* when ended with success 
     */
    public function decrypt(string $cipher, bool $isBase64encoded, ?string $key = null): ?string;

    /**
     * @param string $string is a string which one You would like to encrypt
     * @param bool $isReturnAsBase64encode if You need to get base64 format after encryption set *true*
     * @param null|string $key if You would like to use custom key to encrypt
     * 
     * @return null|string *null* when encryption failed and *string* when ended with success
     */
    public function encrypt(string $string, bool $isReturnAsBase64encode, ?string $key = null): ?string;
}