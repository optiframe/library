<?php

declare(strict_types=1);

namespace OptiFrame\Library\Logs;

use OptiFrame\Library\Logs\Object\LogCollection;

class Logs
{
    private static array $collections = [];

    /**
     * @param array $collections ["info", "events", "errors"]
     */
    public function __construct(array $collections = [])
    {
        foreach($collections as $collection) {
            static::$collections[$collection] = new LogCollection();
        }
    }

    public function __get(string $key): LogCollection
    {
        if (array_key_exists($key, static::$collections)) {
            return static::$collections[$key];
        }
        return static::$collections[$key] = new LogCollection();
    }

    public function save(): bool
    {
        return (new LogsCollector(static::$collections))->saveLog();
    }
}