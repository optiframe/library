<?php

declare(strict_types=1);

namespace OptiFrame\Library\Parser;

class Placeholder
{
    private array $placeholders = [];

    public function __construct(
        private string $pattern,
        private string $subject
    ) {
        $this->parseString();
    }

    public function getPlaceholders(): array
    {
        return $this->placeholders;
    }

    private function parseString(): void
    {
        if (!preg_match_all('/(\}|^)([^\{\}\n]+)\{/', $this->pattern, $strings)) {
            return;
        }
        $strings = array_map(function($callback){return trim($callback, "{,}");}, $strings[0]);
        preg_match_all('/\{[^\{\}]+\}/', $this->pattern, $pattern);

        foreach ($strings as $id => $string) {
            $placeholder = trim($pattern[0][$id], "{,}");
            if (isset($prevString) && isset($prevPlaceholder)) {
                foreach (explode($prevString, $this->subject) as $pString) {
                    if ('' === $pString) {
                        continue;
                    }
                    $this->placeholders[$prevPlaceholder] = explode($string, $pString)[0];
                }
            }

            $prevString = $string;
            $prevPlaceholder = $placeholder;
        }

        if (isset($prevString)) {
            foreach (explode($prevString, $this->subject) as $pString) {
                if ('' === $pString) {
                    continue;
                }
                $this->placeholders[$placeholder] = explode($string, $pString)[0];
            }
        }
    }
}