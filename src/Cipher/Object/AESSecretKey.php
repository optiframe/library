<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Object;

use OptiFrame\Library\Entity\File;
use OptiFrame\Library\Exception\Exception;

class AESSecretKey
{
    private File $file;
    private string $secretKey;

    private const PATH_TO_KEY = '/var/keys/aes.key';

    public function __construct(?string $secretKey = null)
    {
        $this->secretKey = $secretKey ?? $this->getSecretKeyFromFile();
    }

    public function __toString(): string
    {
        return $this->secretKey;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    private function getSecretKeyFromFile(): ?string
    {
        $this->file = new File(self::PATH_TO_KEY);
        if (!$this->file->isExists()) {
            $this->createNewKey();
        }
        if($key = $this->file->getContent()) {
            return base64_decode($key);
        };
        return null;
    }

    private function createNewKey(): void
    {
        $secretKey = random_bytes(256);
        $this->file->setContent(base64_encode($secretKey));
        if (!$this->file->putContent()) {
            throw new Exception('Secret key could not be created due to file write error');
        }
        $this->secretKey = $secretKey;
    }
}