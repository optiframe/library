<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Provider;

class AESProvider implements CipherProviderInterface
{
    private string $secretKey;
    private string $cipherAlgorithm;
    private int $ivLen;

    public function __construct(string $cipherAlgorithm, string $secretKey)
    {
        $this->secretKey = $secretKey;
        $this->cipherAlgorithm = $cipherAlgorithm;
        $this->ivLen = openssl_cipher_iv_length($cipherAlgorithm);
    }

    /**
     * @param string $cipher is a byte/base64 string which You want to decrypt
     * @param bool $isBase64encoded is flag. If true cipher first be encoded
     * @param null|string $key if You would like to use custom key to decrypt
     * 
     * @return null|string *null* when decryption failed and *string* when ended with success 
     */
    public function decrypt(string $cipher, bool $isBase64encoded, ?string $key = null): ?string
    {
        if ($isBase64encoded) {
            $cipher = base64_decode($cipher);
        }
        list($iv, $cipher) = $this->extractIv($cipher);
        if ($string = openssl_decrypt($cipher, $this->cipherAlgorithm, $key ?? $this->secretKey, 0, $iv)) {
            return $string;
        }
        return null;
    }

    /**
     * @param string $string is a string which one You would like to encrypt
     * @param bool $isReturnAsBase64encode if You need to get base64 format after encryption set *true*
     * @param null|string $key if You would like to use custom key to encrypt
     * 
     * @return null|string *null* when encryption failed and *string* when ended with success
     */
    public function encrypt(string $string, bool $isReturnAsBase64encode, ?string $key = null): ?string
    {
        $iv = $this->getIv();
        if ($cipher = openssl_encrypt($string, $this->cipherAlgorithm, $key ?? $this->secretKey, 0, $iv)) {
            $cipher = $this->hideIv($cipher, $iv);
            return $isReturnAsBase64encode ? base64_encode($cipher) : $cipher;
        }
        return null;
    }

    private function getIv(?string $cipher = null): string
    {
        if (!$cipher) {
            return random_bytes($this->ivLen);
        }
    }

    private function extractIv(string $cipher): array
    {
        if ($this->ivLen % 2 === 0) {
            $length = $this->ivLen / 2;
            $pos = 0 - $length;
            return [
                substr($cipher, 0, $length) . substr($cipher, $pos),
                substr($cipher, $length, $pos)
            ];
        }
        $pos = 0 - $this->ivLen;
        return [
            substr($cipher, $pos),
            substr($cipher, 0, $pos)
        ];
    }

    private function hideIv(string $cipher, string $iv): string
    {
        if ($this->ivLen % 2 === 0) {
            $length = $this->ivLen / 2;    
            return substr($iv, 0, $length) . $cipher . substr($iv, $length, $length);
        }
        return $cipher . $iv;
    }

}