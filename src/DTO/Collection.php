<?php

declare(strict_types=1);

namespace OptiFrame\Library\DTO;

class Collection
{
    private int $count;
    private array $items;

    public function __construct(array $items = [], ?int $count = null)
    {
        $this->items = $items;
        $this->count = $count ?? count($items);
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}