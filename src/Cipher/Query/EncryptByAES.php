<?php

declare(strict_types=1);

namespace OptiFrame\Library\Cipher\Query;

use OptiFrame\Library\Interface\QueryInterface;

class EncryptByAES implements QueryInterface
{
    private string $string;
    private bool $isReturnAsBase64encode;
    private ?string $key;

    private const HANDLER = \OptiFrame\Library\Cipher\Handler\EncryptByAESHandler::class;

    public function __construct(
        string $string,
        bool $isReturnAsBase64encode,
        ?string $key = null
    )
    {
        $this->string = $string;
        $this->isReturnAsBase64encode = $isReturnAsBase64encode;
        $this->key = $key;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function isReturnAsBase64encode(): bool
    {
        return $this->isReturnAsBase64encode;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }
}