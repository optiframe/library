# OptiFrame Library #

Biblioteka OptiFrame zawiera zbiór metod, klas i plików stosowanych przez framework OptiFrame.

### Wymagania ###

 * **PHP ^8.0.5**.

### Instalacja ###

Wykonaj metodę `composer create-project optiframe/skeleton`, aby pobrać framework OptiFrame, który w swoich zależnościach ma wpisaną niniejszą bibliotekę i ją pobierze.

Gdybyś jednak chciał pobrać samą bibliotekę, to wykonaj metodę `composer install optiframe/library`, co w efekcie spowoduje pobranie niniejszego repozytorium i umieszczenie go w katalogu `./vendor/optiframe/library/*`.

Zalecam używanie ostatniej numerycznej wersji.

### Pierwsze uruchomienie ###

Oczywiście najlepiej wykorzysytywać gotowy framework, ale nie musisz tego robić. Najważniejsze, abyś w swoim pliku wejściowym wykonał następujący kod:

> `define('APP_START', $_SERVER['REQUEST_TIME_FLOAT'] ?? microtime(true));`
>
> `define('APP_PATH', substr(__DIR__, 0 , -7)); //Dla katalogu ./public (7 znaków)`
>
> `require_once APP_PATH . '/vendor/autoload.php;`
>
> `$app = new OptiFrame\Library\Application('http'); //zamiast http może być Twój podprogram do obsługi odpowiedzi`
>
> `$app->run('run'); //jest to nazwa metody rozpoczynająca wykonywanie podprogramu, możesz też podać kolejne argumenty, jeśli Twoja metoda ich wymaga`
> 
> `$app->terminate(); //metoda zwraca odpowiedź`

Potem już robisz to co chcesz!
### Licencja ###

**MIT** - Darmowe oprogramowanie jest super!