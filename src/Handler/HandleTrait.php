<?php

declare(strict_types=1);

namespace OptiFrame\Library\Handler;

use OptiFrame\Library\Interface\CQRSInterface;
use OptiFrame\Library\Interface\ProviderInterface;

trait HandleTrait
{
    public function handle(CQRSInterface $object, ProviderInterface $provider)
    {
        $handler = $object->getHandler();

        return (new $handler($provider))($object);
    }
}